# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 15:45:28 2019

@author: mehedi
"""
import csv
from sklearn.model_selection import cross_validate



import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.metrics import precision_score, recall_score, f1_score

from sklearn.model_selection import cross_val_score

import warnings


import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from matplotlib import rcParams

from sklearn.neighbors import KNeighborsClassifier
import sys
import time

#from yellowbrick.classifier import ConfusionMatrix
warnings.filterwarnings("ignore")


if __name__ == "__main__":
    global cross
    cross = 10
    
    Type= "_binary_class"
    dataset='CICIDS'
    output_path= '../results/'
    input_file= "../feature/"+dataset+"_feature_"
    
    print("**** "+ dataset+Type+" ****")

    vecsize = list(np.arange(1,3,1))
#    vecsize = list(np.arange(5,10,5))
    knn=[]
#    svm_1=[]
    
    for v in vecsize:
        start1 = time.clock()

        data = pd.read_csv(input_file+str(v)+'.csv')
#        data = pd.read_csv('CICIDS_feature_all_class_'+str(v)+'.csv')
        
        data = data.iloc[np.random.permutation(len(data))]
            # reset the index
        data = data.reset_index(drop=True)
        
        print(data.shape)
        
#        data= data.loc[:1000]
         
        #new version
        sz = data.shape 
        last_data_column = sz[1]-1
        
        #separate label
        X_train = data.iloc[:,0:last_data_column]
        y_train = data.Label
        
        print(X_train.shape, y_train.shape)
        
        knn=[]
        header=["k", 'Accuracy', 'Precision', 'Recall','F1']

        f=open(output_path+dataset+"_classification_time_vecsize_"+str(v)+".txt", 'w')
        for k in range(1):
            k=3
            start = time.clock()
            print("Cross validating for vecsize: ", v, "\tk: ", k)
            #cross validation
    
            scoring = ['accuracy', 'precision_macro', 'recall_macro','f1_macro']
            clf = KNeighborsClassifier(n_neighbors=k) 
            scores = cross_validate(clf, X_train, y_train, scoring=scoring, cv=cross, return_train_score=True)
            
            precision = scores['test_precision_macro'].mean()
            recall = scores['test_recall_macro'].mean()
            accuracy= scores['test_accuracy'].mean()
            fscore= scores['test_f1_macro'].mean()
    
            
    
            print("\n\n*********Results**********")
            print("\nKNN Results for: ", str(k))
            print("\tAccuracy: ", accuracy)
            print("\tPrecision: ", precision)
            print("\tRecall: ", recall)
            print("\tFscore:", fscore)
            #saving the results
            col=[]
            col.append(k)
            col.append(accuracy)
            col.append(precision)
            col.append(recall)
            col.append(fscore)
    
            knn.append(col)
            end = time.clock()
            print("time: ", (end-start), " seconds\n\n")
            t=str((end-start))
            f.write(t+"\n")
            
        with open(output_path+dataset+'_KNN_Result_vecsize_'+str(v)+'.csv', 'w') as writeFile:
            writer = csv.writer(writeFile)
            writer.writerow(header)
            writer.writerows(knn)
        writeFile.close()
        f.close()
    print("**** "+ dataset+Type+" ****")

        
