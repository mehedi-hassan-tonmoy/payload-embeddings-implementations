# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 14:32:40 2019

@author: mehedi
"""
import gensim
import numpy as np
import pandas as pd
import csv
import sys
import time

if __name__ == "__main__":
    
    #2nd part of the main dataset is used here
    if len(sys.argv) != 2:
        print("**** wrong argument ****")
        print("type 1 for binary, type 2 for multiclass")
        sys.exit(0)
    
    choice = int(sys.argv[1])
    print(choice)
    if choice==1:
        Type="_binary_class"
    elif choice==2:
        Type="_multi_class"
    else:
        print("wrong choice!!")
        sys.exit(1)

    input_path='../datasets/'
    model_path='../model/'
    output_path= '../feature/'
    dataset='CICIDS'

    filename = input_path+ dataset+  '_Dataset_part_2'+'.csv'    

    data= pd.read_csv(filename)
    print(filename)
  
    data = data.iloc[:100,:]
    
    size= list(np.arange(1,3,1))
    f=open(output_path+dataset+"_feature_time.txt", 'w')
    for s in size:
        print("for size: ", s)  
        #load the model
        model = gensim.models.KeyedVectors.load_word2vec_format(model_path+dataset+"_model_vecsize_"+str(s))

        row=[] 
        for num in range(s):    
            row.append('feature_'+ str(num+1))
        row.append("Label")     
        
        #open file to save vectors
        filename= '../feature/CICIDS_feature_1.csv'
        filename= output_path+dataset+'_feature_'+str(s)+'.csv'
        with open(filename, 'w') as writeFile:           
    
            writer = csv.writer(writeFile)
            writer.writerow(row)
            
            Label=list(data.Label)
            
            j=0

            start=time.clock()
            for d in data.Payload:
                
                d=d[1:]
                d=d.split(" ")
                
                temp_vec= s* [0]
    
                for i in range(len(d)):
                    temp_vec= temp_vec + model[str(d[i])]
                
                mean_vec= temp_vec/len(d)
                
                
                file = mean_vec
                class_label=Label[j]
                                
                file = np.append(file, class_label)
                writer.writerow(file)

                
                j+=1
            end=time.clock()
        writeFile.close() 
        
        print("Time taken:\t", (end-start), " seconds\n")
        t=str((end-start))
        f.write(t+"\n")
    
    
    print(j) 
    print("Saving in: ", output_path)
    f.close()
#    writeFile.close() 
#    writeFile1.close()
    
                
                
            
    
