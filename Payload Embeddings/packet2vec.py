# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 16:37:17 2019

@author: mehedi
"""
#import os
#from nltk.tokenize import word_tokenize
import gensim
#import time
import numpy as np
import pandas as pd
import sys
import time
import csv
#import simplejson 

if __name__ == "__main__":
    start1=time.clock()
    nan_count = 0

    # Divided main dataset into two equal parts. First part is used here.
    input_path='../datasets/'
    output_path= '../model/'
    dataset='CICIDS'
    data = 'CICIDS_Dataset_part_1.csv'  
    print(data, " loading!!!!!!!!!!!!!")
    
    content = pd.read_csv(data)
    print("Shape: ", content.shape)
    content = content.iloc[:100,:]
    
    print(content['Label'].value_counts())
#    print(data)

    vocab = []
    count=0

    #vocabulary generation:
    
    for payload in content.Payload:

        if str(payload) != 'nan':
            payload=payload[1:]
            payload=payload.split(" ")

            vocab.append(payload)
        else:
            #print('Nan found and skipped!')
            nan_count += 1

    print('data loaded!')
    print("No of nan found: ", nan_count)

    #sys.exit()
    
    #window size set to 5 for all feature size

    size_array = np.arange(1,3,1) 

    f=open(output_path+dataset+"_model_time.txt", 'w')
    
    for size in size_array:
        start=time.clock()
        print("For size: ", size)
        model = gensim.models.Word2Vec(vocab,size=10,window=size,min_count=1,workers=4)
        model.wv.save_word2vec_format(output_path+dataset+"_model_vecsize_"+str(size))
        end=time.clock()
        
        print("Time taken:\t", (end-start), " seconds\n")
        t=str((end-start))
        f.write(t+"\n")
    end1=time.clock()
    f.close()
    
    print("Total Time taken:\t", (end1-start1), " seconds\n")
    print("saved in: ", output_path)
    print("Done")

    
#    model.save("test_w2v_forensics_model")
