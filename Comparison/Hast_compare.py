#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 12:52:38 2020

@author: mehedi
"""

import numpy as np
import pandas as pd
import collections


hast_path="./HAST/"
dataset="CSIC"
input_path="./final_datasets/"

data = pd.read_csv(input_path+dataset+"_binary_class_Label_Payload.csv") 

data = data.iloc[np.random.permutation(len(data))]
data = data.reset_index(drop=True)

data=data.iloc[:10,:]
print(data)

X_data=data.Payload
y=data.Label

sessions=[]
labels=[]
session_lists  = X_data
sessions.extend(session_lists)
for i in range(5):
    print(sessions[i])

label_lists = data.Label   
labels.extend(label_lists)    

import warnings
warnings.filterwarnings("ignore")
from keras.models import Model
from keras.layers import Dense, Input, Dropout, MaxPooling1D, Conv1D, GlobalMaxPool1D, Activation
from keras.layers import LSTM, Lambda, Bidirectional, concatenate, BatchNormalization
from keras.layers import TimeDistributed
from keras.optimizers import Adam
from keras.utils import np_utils
import keras.backend as K
import numpy as np
import tensorflow as tf
from timeit import default_timer as timer
import time
import keras.callbacks
from keras.utils import np_utils
#
LSTM_UNITS = 92
MINI_BATCH = 10
TRAIN_STEPS_PER_EPOCH = 10
VALIDATION_STEPS_PER_EPOCH = 10

dict_5class = {0:'Normal', 1:'Attack'}
def update_confusion_matrix(confusion_matrix, actual_lb, predict_lb):
    for idx, value in enumerate(actual_lb):
        
        p_value = predict_lb[idx]
#        print(idx, value, p_value)
        try:
            confusion_matrix[value, p_value] += 1
        except:
            print(idx, value, p_value)
    return confusion_matrix

#    for i in range(len(actual_lb)):
#        confusion_matrix[actual_lb[i], predict_lb[i]] += 1
#    return confusion_matrix

def truncate(f, n):
    trunc_f = np.math.floor(f * 10 ** n) / 10 ** n
    return '{:.2f}'.format(trunc_f) # only for 0.0 => 0.00


def byte_block(in_layer, nb_filter=(64, 100), filter_length=(3, 3), subsample=(2, 1), pool_length=(2, 2)):
    block = in_layer
    for i in range(len(nb_filter)):
        block = Conv1D(filters=nb_filter[i],
                       kernel_size=filter_length[i],
                       padding='valid',
                       activation='tanh',
                       strides=subsample[i])(block)
        # block = BatchNormalization()(block)
        # block = Dropout(0.1)(block)
        if pool_length[i]:
            block = MaxPooling1D(pool_size=pool_length[i])(block)

    # block = Lambda(max_1d, output_shape=(nb_filter[-1],))(block)
    block = GlobalMaxPool1D()(block)
    block = Dense(128, activation='relu')(block)
    return block

def binarize(x, sz=256):
    return tf.to_float(tf.one_hot(x, sz, on_value=1, off_value=0, axis=-1))

def binarize_outshape(in_shape):
    return in_shape[0], in_shape[1], 256



def mini_batch_generator(sessions, labels, indices, batch_size):
    Xbatch = np.ones((batch_size, PACKET_NUM_PER_SESSION, PACKET_LEN), dtype=np.int64) * -1
    Ybatch = np.ones((batch_size,5), dtype=np.int64) * -1
    batch_idx = 0
    while True:
        print("Number of index: ", indices)
        for idx in range( indices):
            for i, packet in enumerate(sessions[idx]):
                if i < PACKET_NUM_PER_SESSION:
                    for j, byte in enumerate(packet[:PACKET_LEN]):
                        if byte!=' ' :
                            
                            Xbatch[batch_idx, i, (PACKET_LEN - 1 - j)] = byte            
            Ybatch[batch_idx] = labels[idx]
            batch_idx += 1
            if batch_idx == batch_size:
                batch_idx = 0
#                print("Index: ", idx, "Xbatch size: ", len(Xbatch), "Ybath size: ", len(Ybatch))
                yield (Xbatch, Ybatch)
                
                
arg_list = [[1000,14]]
# arg_list = [[100,6]]
for arg in arg_list:
    PACKET_LEN = arg[0]
    PACKET_NUM_PER_SESSION = arg[1]
    TRAIN_EPOCHS = 1

    
#    test_normal_indices = np.random.choice(normal_indices, int(len(normal_indices)*0.4))
#    test_attack_indices = np.concatenate([np.random.choice(attack_indices, int(len(attack_indices)*0.4)) ])
#    test_indices = np.concatenate([test_normal_indices, test_attack_indices]).astype(int)
#    train_indices = np.array(list(set(np.arange(len(labels))) - set(test_indices)))
    train_indices= np.math.ceil((len(data))*0.8)
    test_indices=(len(data))-train_indices
    train_data_generator  = mini_batch_generator(sessions, labels, train_indices, MINI_BATCH)
    val_data_generator    = mini_batch_generator(sessions, labels, test_indices, MINI_BATCH)
    test_data_generator   = mini_batch_generator(sessions, labels, test_indices, MINI_BATCH)

    
    print("preparing mdoel")
    session = Input(shape=(PACKET_NUM_PER_SESSION, PACKET_LEN), dtype='int64')
    input_packet = Input(shape=(PACKET_LEN,), dtype='int64')
    embedded = Lambda(binarize, output_shape=binarize_outshape)(input_packet)
    block2 = byte_block(embedded, (128, 256), filter_length=(5, 5), subsample=(1, 1), pool_length=(2, 2))
    block3 = byte_block(embedded, (192, 320), filter_length=(7, 5), subsample=(1, 1), pool_length=(2, 2))
    packet_encode = concatenate([block2, block3], axis=-1)
    # packet_encode = Dropout(0.2)(packet_encode)
    encoder = Model(inputs=input_packet, outputs=packet_encode)
    encoder.summary()
    encoded = TimeDistributed(encoder)(session)
    lstm_layer = LSTM(LSTM_UNITS, return_sequences=True, dropout=0.1, recurrent_dropout=0.1, implementation=0)(encoded)
    lstm_layer2 = LSTM(LSTM_UNITS, return_sequences=False, dropout=0.1, recurrent_dropout=0.1, implementation=0)(lstm_layer)
    dense_layer = Dense(5, name='dense_layer')(lstm_layer2)
    output = Activation('sigmoid')(dense_layer)
    model = Model(outputs=output, inputs=session)
    model.summary()
    
    script_name = "Hast"
    weight_file =  "./HAST/"+script_name + '_' + str(PACKET_LEN) + '_' + str(PACKET_NUM_PER_SESSION) + '_{epoch:02d}_{val_loss:.2f}.hdf5'
    check_cb = keras.callbacks.ModelCheckpoint(weight_file, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=True, mode='min')
    earlystop_cb = keras.callbacks.EarlyStopping(monitor='val_loss', patience=5, verbose=0, mode='auto')
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    train_time = 0
    start_train = timer()

    model.fit_generator(
        generator=train_data_generator, 
        steps_per_epoch=TRAIN_STEPS_PER_EPOCH,
        epochs=TRAIN_EPOCHS,
        callbacks=[check_cb, earlystop_cb],
        validation_data=val_data_generator,
        validation_steps=VALIDATION_STEPS_PER_EPOCH)
    end_train = timer()
    train_time = end_train - start_train

    # test model
    start_test = timer()
    test_steps = np.math.ceil(test_indices / MINI_BATCH)
    predictions = model.predict_generator(test_data_generator, steps=test_steps)
    end_test = timer()
    test_time = end_test - start_test
    
    predicted_labels = np.argmax(predictions, axis=1)
#    print("predicted_labels\n",predicted_labels)
    try:
        print(len(predicted_labels))
    except:
        print("no len(predicted_labels)")
    true_labels = labels[train_indices:]
#    print(true_labels)
    try:
        
        print(len(true_labels))
    except:
        print("no len(true_labels)")
#    if len(predicted_labels) > len(true_labels):
#        num_pad = len(predicted_labels) - len(true_labels)
#        true_labels = np.concatenate([true_labels, true_labels[0:num_pad]])
#    print (len(predicted_labels))
#    print (len(true_labels))
#    len_test = len(true_labels)
    cf_ma = np.zeros((2,2), dtype=int)
    
    update_confusion_matrix(cf_ma, true_labels, predicted_labels)
    print("\n\ncf_ma\n",cf_ma)
    metrics_list = []
    for i in range(2):
        if i == 0:
            metrics_list.append([dict_5class[i], str(i), str(cf_ma[i,0]), str(cf_ma[i,1]), '--', '--', '--'])
        else:
            acc = truncate((float(test_indices-cf_ma[:,i].sum()-cf_ma[i,:].sum()+cf_ma[i,i]*2)/test_indices)*100, 2)
            tpr = truncate((float(cf_ma[i,i])/cf_ma[i].sum())*100, 2)
            fpr = truncate((float(cf_ma[0,i])/cf_ma[0].sum())*100, 2)
            metrics_list.append([dict_5class[i], str(i), str(cf_ma[i,0]), str(cf_ma[i,1]),  str(acc), str(tpr), str(fpr)])
            
    overall_acc = truncate((float(cf_ma[0,0]+cf_ma[1,1])/test_indices)*100, 2)
    overall_tpr = truncate((float(cf_ma[1,1])/cf_ma[1:].sum())*100, 2)
    overall_fpr = truncate((float(cf_ma[0,1:].sum())/cf_ma[0,:].sum())*100, 2)
    
    print(overall_acc)
    print(overall_tpr)
    print(overall_fpr)
    with open(hast_path+dataset+'_hast_result.txt','a') as f:
        f.write("label\tindex\t0\t1\t2\t3\t4\tACC\tTPR\tFPR\n")
        for metrics in metrics_list:
            f.write('\t'.join(metrics) + "\n")
        f.write('Overall accuracy: ' + str(overall_acc) + "\n")
        f.write('Overall TPR: ' + str(overall_tpr) + "\n")
        f.write('Overall FPR: ' + str(overall_fpr) + "\n")
        f.write('Train time(second): ' + str(int(train_time)) + "\n")
        f.write('Test time(second): ' + str(int(test_time)) + "\n\n")