#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 12:18:15 2019

@author: mehedi
"""

#import os
import time
import numpy as np
import pandas as pd
import sys
import csv
import math


from keras import backend as K

from keras.callbacks import TensorBoard
from keras.models import load_model
from keras.models import Model
from keras.layers import Dense, Input, Dropout
from keras.models import model_from_json
from tensorflow import Tensor

#def recall_m(y_true, y_pred):
#    
#    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
#    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
#    recall = true_positives / (possible_positives + K.epsilon())
#    return recall
#
#def precision_m(y_true, y_pred):
#    
#    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
#    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
#    precision = true_positives / (predicted_positives + K.epsilon())
#    return precision
#
#def f1_m(y_true, y_pred):
#    precision = precision_m(y_true, y_pred)
#    recall = recall_m(y_true, y_pred)
#    return 2*((precision*recall)/(precision+recall+K.epsilon()))

if __name__ == "__main__":
    
#    nan_count = 0
#
##     Divided main dataset into two equal parts. First part is used here.
#    
#    data = 'CICIDS_Dataset.csv'     
#    content = pd.read_csv(data)
##    content=content.iloc[:10]
#    print(data, " loaded")
#
#    vocab = []
#    count=0
#
#    #vocabulary generation:
#    
##    for Label, Payload in content:
#    for k in range(len(content)):
#        Label= content.iloc[k][0]
#        if Label!='0' :
#            Label= '1'
#        payload= content.iloc[k][1]
#        array = [0 for i in range(256)]
#
#        if str(payload) != 'nan':
#            payload=payload[1:]
#            payload=payload.split(" ")
##            print(len(payload))
#            for a in payload:
#                array[int(a)]+=1
##            print(array)
#            for j in range(len(array)):
#                array[j]= (array[j]/(len(payload)))
##            array= np.true_divide(array,len(payload))
##            print(array)
#            array.append(Label)
##            sys.exit()
#            vocab.append(array)
##            vocab.append(Label)
#        else:
#            #print('Nan found and skipped!')
#            nan_count += 1
#
#    print('Preprocesssed!')
#    row=[]          
#    for i in range(257):
#        if i!=0:
#            name="Byte "+str(i)
#            
#            row.append(name)
#
#    name="Label"
#    row.append("Label")
##    print(row)
#    print("No of nan found: ", nan_count)
#    with open("CICIDS_Dataset_binary_BF.csv", 'w') as writeFile:
#        writer = csv.writer(writeFile)
#        writer.writerow(row)
#        writer.writerows(vocab)
#    writeFile.close() 
#    test_with_label.csv
    d = 'CICIDS_Dataset_binary_BF.csv' 
    data = pd.read_csv(d)
    print("Data loaded again")

    
    data = data.iloc[np.random.permutation(len(data))]
        # reset the index
    data = data.reset_index(drop=True)
#    data=data.iloc[:2000, :]
    #dataset with features is divided into 80:20 ratio.
        
    ratio = 0.8
    sz = data.shape   
    last_data_column = sz[1]-1
    train = data.iloc[:int(sz[0] * ratio), :]
    test = data.iloc[int(sz[0] * ratio):, :]
    train = train[train['Label']==0]
    X_train = train.iloc[:,0:last_data_column]
    y_train = train.Label
#        
    # separate feature and label 
    X_test = test.iloc[:,0:last_data_column]
    # label column
    y_test = test.Label
    
#    X_train= data.iloc[:, 0:256]
    
    print("\n\n\n\n",X_train.shape,X_test.shape )
    print(y_train.shape,y_test.shape, "\n\n\n\n" )


    hidden_layers = [200,100,200]
    activation_function = "relu" 
    dropout = 0.2
    batch_size=1
   
#    autoencoder = init_model(hidden_layers, activation_function, dropout)
    input_dimension = 256
    input = Input(shape=(input_dimension,))

    encoded = Dense(int(hidden_layers[0]), activation=activation_function)(input)
    encoded = Dropout(dropout)(encoded)

    encoded = Dense(int(hidden_layers[1]), activation=activation_function)(encoded)
    encoded = Dropout(dropout)(encoded)
    
#    encoded = Dense(int(hidden_layers[2]), activation=activation_function)(encoded)
#    encoded = Dropout(dropout)(encoded)

#    decoded = Dense(int(hidden_layers[2]), activation=activation_function)(encoded)
#    decoded = Dropout(0.2)(decoded)

    decoded = Dense(int(hidden_layers[1]), activation=activation_function)(encoded)
    decoded = Dropout(0.2)(decoded)

    decoded = Dense(int(hidden_layers[0]), activation=activation_function)(decoded)
    decoded = Dropout(0.2)(decoded)

    decoded = Dense(input_dimension, activation="sigmoid")(decoded)
    autoencoder = Model(outputs=decoded, inputs=input)
    autoencoder.compile(loss="binary_crossentropy", optimizer="adadelta", metrics=['accuracy'])
    
    #training error
    autoencoder.fit(X_train, X_train,shuffle=True, batch_size=1,epochs=10, verbose=1)
    decoded_X=autoencoder.predict(X_train)
    
    errors_list_1 = np.mean((decoded_X - X_train) ** 2, axis=1)
    
    mean = np.mean(errors_list_1)
    stdev = np.std(errors_list_1)
    
    qs = np.percentile(errors_list_1, [100, 75, 50, 25, 0])
    iqr = qs[1] - qs[3]
    MC = ((qs[0]-qs[2])-(qs[2]-qs[4]))/(qs[0]-qs[4])
    if MC >= 0:
        constant = 3
    else:
        constant = 4
    iqrplusMC = 1.5 * math.pow(math.e, constant * MC) * iqr
    
    median = np.median(errors_list_1)
    mad = np.median([np.abs(e - median) for e in errors_list_1])
    
    zscore = [0 for i in range(len(errors_list_1))]
    zscore = [0.6745 * (e - median) / mad for e in errors_list_1]
    
    #testing
    
    decoded_X=autoencoder.predict(X_test)
    errors_list = np.mean((decoded_X - X_test) ** 2, axis=1)
    
    #means/stdev
    decision_mean=[]
    decision_iqr=[]
    for e in errors_list:
        if e > (mean + (2 * stdev)):
            decision_mean.append(1)
        else:
            decision_mean.append(0)
            
        if e > (qs[1]+ iqrplusMC):
            decision_iqr.append(1)
        else:
            decision_iqr.append(0)
    #iqr
    
    f = open('./Evaluation/scores_output_with_normal_training.txt','w')
    f2 = open('./Results/performance_with_normal_training.txt','w')
    f2.write('Filename' +'\t'+'Accuracy' +'\t'+ 'Precision'+'\t'+'Recall'+'\t'+'F1'+'\t'+'FPR'+'\n')
    f.write('Filename' +'\t'+'TP' +'\t'+ 'FP'+'\t'+'FN'+'\t'+'TN'+'\n')
    
    TP=0
    FP=0
    FN=0
    TN=0
    for k in range(len(y_test)):
        if y_test.iloc[k]==1 and decision_mean[k]==1:
            TP+=1
        elif y_test.iloc[k]==1 and decision_mean[k]==0:
            FN+=1
        elif y_test.iloc[k]==0 and decision_mean[k]==1:
            FP+=1
        elif y_test.iloc[k]==0 and decision_mean[k]==0:
            TN+=1
    f.write("Mean" + '\t\t'+ str(TP) +'\t'+ str(FP) +'\t'+ str(FN) +'\t'+ str(TN)+"\n" )
    
    accuracy= (TP+TN)/ (TP+TN+FP+FN)
    precision= TP/(TP+FP)
    recall= TP/(TP+FN)
    f1= 2 * precision * recall / (precision+recall)
    FPR= FP/(TN+FP)
    f2.write("Mean" + '\t\t'+ str(round(accuracy,2)) +'\t\t'+ str(round(precision,2)) +'\t\t'+ str(round(recall, 2)) +'\t'+ str(round(f1,2))+'\t'+ str(round(FPR,2))+"\n" )

    
    TP=0
    FP=0
    FN=0
    TN=0
    for k in range(len(y_test)):
        if y_test.iloc[k]==1 and decision_iqr[k]==1:
            TP+=1
        elif y_test.iloc[k]==1 and decision_iqr[k]==0:
            FN+=1
        elif y_test.iloc[k]==0 and decision_iqr[k]==1:
            FP+=1
        elif y_test.iloc[k]==0 and decision_iqr[k]==0:
            TN+=1
    f.write("IQR" + '\t\t'+ str(TP) +'\t'+ str(FP) +'\t'+ str(FN) +'\t'+ str(TN)+"\n"  )
    
    accuracy= (TP+TN)/ (TP+TN+FP+FN)
    if (TP+FP) ==0:
        precision=0
    else:
        precision= TP/(TP+FP)
    recall= TP/(TP+FN)
    if (precision+recall) ==0:
        f1=0
    else:
        f1= 2 * precision * recall / (precision+recall)
    FPR= FP/(TN+FP)
    f2.write("IQR" + '\t\t'+ str(round(accuracy,2)) +'\t\t'+ str(round(precision,2)) +'\t\t'+ str(round(recall, 2)) +'\t'+ str(round(f1,2))+'\t'+ str(round(FPR,2))+"\n" )
    #zscore check
    for a in range(1,4,1):
        decision=[]
        if a==1:
            for m in range(len(zscore)):
                if zscore[m] > 3.5 or zscore[m] < -3.5:
                    decision.append(1)
                else:
                    decision.append(0)
        elif a==2:
            for m in range(len(zscore)):
                if zscore[m] > 3.0 or zscore[m] < -3.0:
                    decision.append(1)
                else:
                    decision.append(0)
        elif a==3:
            for m in range(len(zscore)):
                if zscore[m] > 2.5 or zscore[m] < -2.5:
                    decision.append(1)
                else:
                    decision.append(0)

        TP=0
        FP=0
        FN=0
        TN=0
        for k in range(len(y_test)):
            if y_test.iloc[k]==1 and decision[k]==1:
                TP+=1
            elif y_test.iloc[k]==1 and decision[k]==0:
                FN+=1
            elif y_test.iloc[k]==0 and decision[k]==1:
                FP+=1
            elif y_test.iloc[k]==0 and decision[k]==0:
                TN+=1
     
        print("Done")

        f.write("Z-score_" +str(a)+ '\t'+ str(TP) +'\t'+ str(FP) +'\t'+ str(FN) +'\t'+ str(TN) +"\n")
        accuracy= (TP+TN)/ (TP+TN+FP+FN)
        precision= TP/(TP+FP)
        recall= TP/(TP+FN)
        f1= 2 * precision * recall / (precision+recall)
        FPR= FP/(TN+FP)
        f2.write("Z-score_" +str(a)+ '\t'+ str(round(accuracy,2)) +'\t\t'+ str(round(precision,2)) +'\t\t'+ str(round(recall, 2)) +'\t'+ str(round(f1,2))+'\t'+ str(round(FPR,2))+"\n" )


    result= y_test.value_counts()
    f.write(str(result) )
    f2.write(str(result) )
    f.close()
    f2.close()
    print("\n\n\n\n",X_train.shape,X_test.shape )
    print(y_train.shape,y_test.shape, "\n\n\n\n" )
    print(y_test.value_counts())


