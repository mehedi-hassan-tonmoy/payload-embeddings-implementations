#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 14:29:15 2019

@author: mehedi
"""
#import os
#import glob
import pandas as pd
import numpy as np

from keras import backend as K
#from functools import partial
from keras.models import Sequential
from keras.layers import Flatten, Conv1D, MaxPooling1D, Dropout, Dense
from keras.utils import to_categorical
#
from keras.layers import BatchNormalization
from sklearn.model_selection import train_test_split
#import statistics 
#from sklearn.preprocessing import LabelEncoder




#from keras.callbacks import EarlyStopping
#from keras.callbacks import ModelCheckpoint
#
#from keras.models import load_model

import sys
LABELS = {}
counter = iter(range(20))




def recall_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

def precision_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

def pad_and_convert(s):
    """Collect 1000 bytes from packet payload. If payload length is less than
    1000 bytes, pad zeroes at the end. Then convert to integers and normalize."""
    
    
    s=s.split(" ")

    s=s[1:]
    
    if len(s) < 1460:
        s += '0' * (1460-len(s))
    else:
        s = s[:1460]
   
    return [float(int(s[i], 16)/255) for i in range(0, 1460, 1)]

#def read_file(f, label):
#    df = pd.read_csv(f, index_col=None, header=0)
#    df.columns = ['data']
#    df['label'] = label
#    return df

#def preprocess(path):
#    files = glob.glob(os.path.join(path, '*.txt'))
#    
#    print(files)
#    list_ = []
#    for f in files:
#        label = f.split('/')[-1].split('.')[0]
#        LABELS[label] = next(counter)
#        labelled_df = partial(read_file, label=LABELS[label])
#        list_.append(labelled_df(f))
#    df = pd.concat(list_, ignore_index=True)
#
#    return df

#def main():
if __name__ == '__main__':
    activation = 'relu'
#    df = preprocess('Dataset')
    fp="CICIDS_Dataset_binary.csv"
    print(fp, " Loading...........")
    df = pd.read_csv(fp)
    
    print("......Loaded........")
    df = df.iloc[np.random.permutation(len(df))]
    # reset the index
    df = df.reset_index(drop=True)
#    df=df.loc[:50000]
#    length=[]
#    index=[]
##    df= df[df['Label']!='0']
#    i=0
#    for payload in df.Payload:
#        payload=payload[1:]
#        payload=payload.split(" ")
##        print("\nPayloads: \n\n", payload)
#        length.append(len(payload))
#        if len(payload) >1460:
#            index.append(i)
#        i+=1
#    labels=[]        
#    for j in index:
#        labels.append(df.iloc[j][0])
#    print(set(labels))
#    
#    print(labels.count('0'))
#    print(labels.count('Bot'))
#    print(labels.count('DDoS'))
#    
#    print("Min: ", min(length))
#    print("Max: ", max(length))
#    print("Mean: ", np.mean(length))
#    print("STD: ", np.std(length))
    
#    
#    labels= list(df.Label)
#    labels= set(labels)
    
    num_classes=2
    df['Payload'] = df['Payload'].apply(pad_and_convert)
#    lb_make = LabelEncoder()
#    df['Label'] = lb_make.fit_transform(df['Label'])
            
#    print("labelelling")
#    for i in range(len(df)):
#        if (df.iloc[i][0]!='0') :
#            df.iloc[i , 0] ='1'
#            
    
#    sys.exit()
    print("200, 3, 5\n200, 200, 3, 4, Softmax")
    X_train, X_test, y_train, y_test = train_test_split(df['Payload'], df['Label'], test_size=0.2, random_state=4)

    X_train = X_train.apply(pd.Series)
    X_test = X_test.apply(pd.Series)
    print("Shape before:  ", X_train.shape)
    
    X_train = X_train.values.reshape(X_train.shape[0], X_train.shape[1], 1)
    X_test = X_test.values.reshape(X_test.shape[0], X_test.shape[1], 1)
    y_train = to_categorical(y_train, num_classes)
    y_test = to_categorical(y_test, num_classes)
    
    print("Shape:  ", X_train.shape, X_train.shape[1:])
#    sys.exit()
    model = Sequential()
    
    model.add(Conv1D(200, strides=3, input_shape=X_train.shape[1:], activation=activation, kernel_size=4, padding='same'))
    model.add(Dropout(0.05))
    model.add(BatchNormalization())
    
    model.add(Conv1D(200, strides=1, activation=activation, kernel_size=5, padding='same'))
    model.add(Dropout(0.05))
    model.add(BatchNormalization())
    
    model.add(MaxPooling1D(strides=2))
    model.add(Dropout(0.05))
    
    model.add(Flatten())
    
    model.add(Dense(200, activation=activation))
    model.add(Dropout(0.05))
    
    model.add(Dense(100, activation=activation))
    model.add(Dropout(0.05))
    
    model.add(Dense(50, activation=activation))
    model.add(Dropout(0.05))
    
    model.add(Dense(num_classes, activation='softmax'))
    print(model.summary())

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy',f1_m, precision_m, recall_m])
    

    result = model.fit(X_train, y_train, verbose=1, epochs=300, batch_size=64, validation_split=0.2, shuffle=True,)
    

    
    loss, accuracy, f1_score, precision, recall= model.evaluate(X_test,	y_test,verbose=1)
    
    print(loss, accuracy, f1_score, precision, recall)
    
    f = open('.Results/output_2.txt','w')
    f.write("Lets write something\n")
#    f.write( str(p+1)+'\t'+str(round(top_n_scores[p],2))+'\t' +str(top_n_docs[p])+'\t'+str(valid_scores)+'\t'+ str(doc_data[p][1][0])+'\n' )
    f.write("Loss: "+ str(round(loss,2))+"\t"+ "accuracy: "+ str(round(accuracy,2))+"\t"+ "f1_score: "+str(round(f1_score,2))+"\t"+ "precision: "+
            str(round(precision,2))+"\t"+ "recall: "+ str(round(recall,2)))
    f.close()
    
    print("200, 3, 5\n200, 200, 3, 4, Softmax")

