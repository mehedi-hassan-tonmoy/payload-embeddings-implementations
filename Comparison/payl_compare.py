#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 14:07:51 2019

@author: mehedi
"""

import time
import sys
import os
#import pickle as pkl
import numpy as np
import csv
import random
#import configparser
import pandas as pd
#import dpkt
#import socket

import scipy.spatial.distance as dist
import math
#import distance_and_clustering as dc
#import read_pcap as dpr



################ form payl           
def payl(normal_data, payload):
    
#    configFN = sys.argv[1]
#    configFN="payl_http.cfg"
#
#    # Read config file
#    config = configparser.ConfigParser()
#    config.read(configFN)

    # Parse parameters
#    feature_fn = str(config['general']['feature'])
#    model_fn = str(config['general']['model'])

#    protocol = str(config['payl']['type'])
#    smoothing_factor = float(config['payl']['smoothing_factor'])
#    threshold = float(config['payl']['threshold'])
    
    protocol="HTTP"
    smoothing_factor=0.001
    threshold=256

    # If model already exists, quit
#    if os.path.exists(model_fn):
#        sys.stderr.write(('Error. Model file "{0}" already exists.\n'.format(model_fn)))
#        sys.exit(1)

    # Check protocol parameter
    if protocol not in ['HTTP','DNS']:
        sys.stderr.write('Error. "{0}" is an invalid protocol.\n'.format(protocol))
        sys.exit(1)

    sys.stdout.write('Reading features\n')

    # Read in features
#    payload = list()
#    with open(feature_fn,'rb') as fr:
#        n = pkl.load(fr)
#        print(n)
#        for i in range(n):
#            payload.append(pkl.load(fr))

    # Shuffle and split payloads into training/testing sets
    random.shuffle(normal_data)
    print("Normal data: ", len(normal_data))
    thresh = int(len(normal_data)*0.9)
    train = normal_data[:thresh]
    test = normal_data[thresh:]
    
#    print("train Length: ", len(train))
#    print("test Length: ", len(test))
#    for a in train:
#        print("\n\nPayload: \n", a)
#    for a in test:
#        print("\n\nPayload: \n", a)

    # HTTP training set needs at least one min and one max length sample
    if protocol == 'HTTP':
        min_train = min(train,key=len)
        min_test = min(test,key=len)

        if len(min_test) < len(min_train):
            train.append(min_test)
            test.remove(min_test)

        max_train = max(train,key=len)
        max_test = max(test,key=len)

        if len(max_test) > len(max_train):
            train.append(max_test)
            test.remove(max_test)

    # Train model
    model,train_lengths,min_length = train_and_test(train, test, smoothing_factor, threshold)

#    sys.stdout.write('Saving model\n')

    # Store model
#    with open(model_fn,'wb') as fw:
#        pkl.dump(model,fw)
#        pkl.dump(list(train_lengths),fw)
#        pkl.dump(min_length,fw)
#    for a in range(3):
        
    evaluation(payload,model,train_lengths,min_length)
        
        
        
        
################ from analysis     
# Calculates frequency of ASCII characters in string
def get_freq(s):
    rv = [0]*256
    for c in s:
        rv[ord(c)] += 1
    return rv

# Trains and tests clustering
def train_and_test(train, test, sf, thresh):
    # Dictionary keyed on payload lengths
    train_length = dict()

    # Store payloads into dictionary
    
    for label, payload in train:
        l = len(payload)

        if l not in train_length:
            train_length[l] = list()

        train_length[l].append(payload)

    # Get min and max length
    max_length = max(train_length.keys())
    min_length = min(train_length.keys())

    # Create feature vector to store values
    feature_vector = list()
    for i in range(1, max_length-min_length+2):
        mean = [0]*256
        stddev = [0]*256
        feature_vector.append(np.vstack((mean,stddev)).T)

    sys.stdout.write('Training Model\n\n')

    # For each length of payload, calculate the frequency of ASCII characters
    for key in sorted(train_length.keys()):

        # Create frequency array to store values
        freq = [[0]*256]*(len(train_length[key]))

        # Calculate frequency of ASCII characters
        for e,payload in enumerate(train_length[key]):
            freq[e] = get_freq(payload)

        # Store mean and std dev in feature vector for this length payload
        stddev = np.std(freq,axis=0)
        mean = np.mean(freq, axis=0)
        feature_vector[min_length-key] = np.vstack((mean,stddev)).T

    sys.stdout.write('Testing Normal Model\n\n')

    TP = 0
    FP = 0

    # Run testing
    for label,payload in test:
        mahabs_distance = sys.maxsize

        # Get frequency of payload
        freq = get_freq(payload)

        # If we've stored this length of payload before, retrieve the feature vector
        # and calculate the Mahalanobis Distance
        if len(payload) in train_length.keys():
            averaged_feature_vector = (feature_vector[min_length-len(payload)])
            mahabs_distance = mahalanobis_distance(averaged_feature_vector, freq, sf)

        # Compare the distance to the threshold
        if mahabs_distance <= thresh:
            TP += 1 # it's nominal and is classified as nominal
        else:
            FP += 1 # it's nominal, but is classified as anomalous

    sys.stdout.write('Total Number of testing samples: {0}\n'.format(len(test)))
    sys.stdout.write('TPs: {0}    FPs: {1}\n'.format(TP,FP))
    sys.stdout.write('Percentage of True positives: {0}/{1} = {2} %\n'.format(TP,len(test),str((TP/float(len(test)))*100.0)))
    sys.stdout.write('Percentage of False positives: {0}/{1} = {2} %\n'.format(FP,len(test),str((FP/float(len(test)))*100.0)))

    # Return model
    return feature_vector,train_length.keys(),min_length


################from evaluation
def evaluation(payload,model,train_lengths,min_length):
#    model_fn = sys.argv[1]
#    feature_fn = sys.argv[2]
#    smoothing_factor = float(sys.argv[3])
#    threshold = float(sys.argv[4])
    
    
    """"""""
    smoothing_factor = 0.001
    threshold = 256
    """"""""
    
#    sys.stdout.write('Reading model\n')

    # Read model
#    model = None
#    train_lengths = None
#    min_length = None
#    with open(model_fn,'rb') as fr:
#        model = pkl.load(fr)
#        train_lengths = pkl.load(fr)
#        min_length = pkl.load(fr)

#    sys.stdout.write('Reading features\n')

    # Read in features
#    payload = list()
#    with open(feature_fn,'r') as fr:
#        n = pkl.load(fr)
#
#        for i in range(n):
#            payload.append(pkl.load(fr))

    TP = 0
    FP = 0
    TN = 0
    FN = 0

    total_nominal = 0
    total_anomalous = 0

    sys.stdout.write('Testing whole model\n\n')
    start1= time.clock()
    random.shuffle(payload)
    # Test model on features
    for l,p in payload:
        mahabs_distance = sys.maxsize

        # Get frequency of payload
        freq = get_freq(p)

        # If we've stored this length of payload before, retrieve the feature vector
        # and calculate the Mahalanobis Distance
        if len(p) in train_lengths:
            averaged_feature_vector = (model[min_length-len(p)])
            mahabs_distance = mahalanobis_distance(averaged_feature_vector, freq, smoothing_factor)

        # Tally number of nominal and anomalous samples
        if l == '0':
            total_nominal += 1
        elif l == '1':
            total_anomalous += 1

        # Compare the distance to the threshold
        if (mahabs_distance <= threshold) and (l == '0'):
            TP += 1 # it's nominal and is classified as nominal
        elif (mahabs_distance > threshold) and (l == '0'):
            FP += 1 # it's nominal, but is classified as anomalous
        elif (mahabs_distance > threshold) and (l == '1'):
            TN += 1 # it's anomalous and is classified as anomalous
        elif (mahabs_distance <= threshold) and (l == '1'):
            FN += 1 # it's anomalous, but is classified as nominal
    end1= time.clock()
    sys.stdout.write('Total Number of samples: {0}\n'.format(len(payload)))
    sys.stdout.write('Nominal: {0}    Anomalous: {1}\n'.format(total_nominal,total_anomalous))
    sys.stdout.write('TPs: {0}    FPs: {1}    TN: {2}    FN: {3}\n'.format(TP,FP,TN,FN))
    if total_nominal > 0:
        sys.stdout.write('Percentage of True positives: {0}/{1} = {2} %\n'.format(TP,total_nominal,str((TP/float(total_nominal))*100.0)))
    if total_nominal > 0:
        sys.stdout.write('Percentage of False positives: {0}/{1} = {2} %\n'.format(FP,total_nominal,str((FP/float(total_nominal))*100.0)))
    if total_anomalous > 0:
        sys.stdout.write('Percentage of True negatives: {0}/{1} = {2} %\n'.format(TN,total_anomalous,str((TN/float(total_anomalous))*100.0)))
    if total_anomalous > 0:
        sys.stdout.write('Percentage of False negatives: {0}/{1} = {2} %\n'.format(FN,total_anomalous,str((FN/float(total_anomalous))*100.0)))

    accuracy= (TP+TN)/ (TP+TN+FP+FN)
    precision= TP/(TP+FP)
    recall= TP/(TP+FN)
    f1= 2 * precision * recall / (precision+recall)
    FPR= FP/(TN+FP)
    
    time_taken=(end1-start1)/60
    
    print("accuracy: ", accuracy)
    print("precision: ", precision)
    print("recall: ", recall)
    print("f1: ", f1)
    print("FPR: ", FPR, "\n\n")
    
    
    PAYL_path="./PAYL/"
    dataset="CSIC"
    f=open(PAYL_path+dataset+"_PAYL_results.txt", 'a')
    f.write(str(accuracy)+"," + str(precision)+"," +str(recall)+"," +str(f1)+"," +str(FPR)+"\n" )
    f.close()
    
    file=open(PAYL_path+dataset+"_PAYL_CM.txt", 'a')
    file.write(str(TP)+"," +str(FP)+"," +str(FN)+"," +str(TN)+","+str(time_taken)+"\n")
    file.close()
    

################from distnace
        
def mahalanobis_distance(averaged_feature_vector, freq, sf):
    if sf == 0:
        raise Exception('Smoothing factor cannot be zero')

    dist = 0

    # For each ASCII character
    for n in range(0,256):
        xi = averaged_feature_vector[n][0]
        yi = freq[n]
        sigi = averaged_feature_vector[n][1]
        dist += (abs(xi-yi)/(sigi+sf))

    return dist


if __name__ == '__main__':
    
    datastet_path="./final_datasets/"
    PAYL_path="./PAYL/"
    dataset="CSIC"

    filename=datastet_path+dataset+" HTTP Final Dataset.csv"


    print(filename)

    data = pd.read_csv(filename)     
    
    attack= data[(data['Label']==1)]
    normal= data[(data['Label']==0)]
    
    print("Attack: ", attack.shape, "\nNormal: ", normal.shape)
    
    
    normal = normal.iloc[np.random.permutation(len(normal))]
    normal = normal.reset_index(drop=True)
    
    size=int(len(normal)/2)

    part_1= normal.iloc[:size, :]
    normal_train= normal.iloc[size:, :]
    
    
    main_data = pd.concat([part_1, attack], ignore_index=True)
    
    main_data = main_data.iloc[np.random.permutation(len(main_data))]
    main_data.reset_index(drop=True)

    normal_train.reset_index(drop=True)
    
    print("main_data: ", main_data.shape, "\nNormal_train: ", normal_train.shape)
    
    main_data.to_csv(PAYL_path+dataset+"_Main_payl.csv", index=False)
    normal_train.to_csv(PAYL_path+dataset+"_normal_payl.csv", index=False)
#    sys.exit()
    
    
    with open(PAYL_path+dataset+"_Main_payl.csv",'r') as csvfile:
        reader = csv.reader(csvfile)
        main_data= list(reader)
        main_data=main_data[1:]
        
    with open(PAYL_path+dataset+"_normal_payl.csv",'r') as csvfile:
        reader = csv.reader(csvfile)
        normal_train= list(reader)
        normal_train=normal_train[1:]

    print("\n\nPAYL Running.....")
    start=time.clock()
    
    f=open(PAYL_path+dataset+"_PAYL_results.txt", 'w')
    f.write('Accuracy'+"," +'Precision'+"," + 'Recall'+"," +'F1'+"," + 'FPR'+ "\n")
    f.close()
    
    file=open(PAYL_path+dataset+"_PAYL_CM.txt", 'w')
    file.write('TP'+"," +'FP'+"," + 'FN'+"," +'TN'+","+"Time"+"\n")    
    file.close()
    for a in range(3):
        
        payl(normal_train,main_data)
    
    end=time.clock()
    print("Done in ", (end-start)/60 )